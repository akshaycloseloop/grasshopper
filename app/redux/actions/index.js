
export const ADD_OR_UPDATE_LOADING_INDICATOR_BOOLEAN = '';
export const ADD_REGISTER = 'ADD_REGISTER';
export const ADD_LOGIN = 'ADD_LOGIN';
export const ADD_DISPENSARIES = 'ADD_DISPENSARIES';
export const ADD_PRODUCTS = 'ADD_PRODUCTS';

export const addOrUpdateLoadingIndicatorBooleans = (dispatch, indicatorName, value) => {
	dispatch({ type: ADD_OR_UPDATE_LOADING_INDICATOR_BOOLEAN, data: { indicatorName, value } });
};

export const saveRegister = (dispatch, registerData) => {
	dispatch({ type: ADD_REGISTER, data: registerData });
};

export const saveLogin = (dispatch, loginData) => {
	dispatch({ type: ADD_LOGIN, data: loginData });
};

export const saveDispensaries = (dispatch, dispensaries) => {
	dispatch({ type: ADD_DISPENSARIES, data: dispensaries });
};

export const saveProducts = (dispatch, products) => {
	dispatch({ type: ADD_PRODUCTS, data: products });
};
