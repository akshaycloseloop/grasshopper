import {
	ADD_OR_UPDATE_LOADING_INDICATOR_BOOLEAN,
	ADD_REGISTER,
	ADD_LOGIN,
	ADD_DISPENSARIES,
	ADD_PRODUCTS,
} from '../actions';

const addOrUpdateLoadingIndicator = (loading, action) => {
	const newLoadingState = loading;
	newLoadingState[action.data.indicatorName] = action.data.value;
	return loading;
};

let dataState = { loading: {}, dispensariesData: {}, productsData: {} };

const MainReducer = (state = dataState, action) => {
	switch (action.type) {
		case ADD_OR_UPDATE_LOADING_INDICATOR_BOOLEAN:
			dataState = Object.assign(
				{}, state, { loading: addOrUpdateLoadingIndicator(state.loading, action) },
			);
			return dataState;
		case ADD_REGISTER:
			dataState = Object.assign({}, state, { registerData: action.data });
			return dataState;
		case ADD_LOGIN:
			dataState = Object.assign({}, state, { loginData: action.data });
			return dataState;
		case ADD_DISPENSARIES:
			dataState = Object.assign({}, state, { dispensariesData: action.data });
			return dataState;
		case ADD_PRODUCTS:
			dataState = Object.assign({}, state, { productsData: action.data });
			return dataState;
		default:
			return dataState;
	}
};

export default MainReducer;
