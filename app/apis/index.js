/* global fetch:false */
import { NetInfo, Platform, AsyncStorage } from 'react-native';
import webUrls from './constant';
import { printInDebugModeOnly } from '../helper/debugging';
import {
	saveRegister,
	saveLogin,
	addOrUpdateLoadingIndicatorBooleans,
	saveDispensaries,
	saveProducts,
} from '../redux/actions';
import { showAlert } from '../components/alert';
import { screensNames } from '../routes/helper';
import strings from '../constants/strings';
import loadingIndicator from '../components/loading-indicator/loadingIndicator';
import { getAuthorizationToken } from './helper';

const fetchDataUsingPost = async (body, url) => fetch(
	url,
	{
		method: 'POST',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
			Authorization: await getAuthorizationToken(),
		},
		body,
	},
);

const fetchDataUsingGet = async url => fetch(
	url,
	{
		method: 'GET',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
			Authorization: await getAuthorizationToken(),
		},
	},
);

/*
NOTE: Use this code to write new APIs. Add API call in if block.
NetInfo.isConnected.fetch().then((isConnected) => {
	if (isConnected) {

	} else {
		showAlert(strings.common.networkError);
	}
});
*/

const fetchProducts = (dispatch) => {
	// TODO: Replace these with real values.
	const dispensaryId = '1';
	const locationId = '1';
	const body = `dispensary_id=${dispensaryId}&location_id=${locationId}`;
	const url = `${webUrls.products}?${body}`;
	NetInfo.isConnected.fetch().then((isConnected) => {
		if (isConnected) {
			addOrUpdateLoadingIndicatorBooleans(dispatch, loadingIndicator.productsData.main, true);
			fetchDataUsingGet(url)
				.then(response => response.json())
				.then((responseJson) => {
					addOrUpdateLoadingIndicatorBooleans(
						dispatch,
						loadingIndicator.productsData.main,
						false,
					);
					if (responseJson.data) {
						saveProducts(dispatch, responseJson.data);
					} else {
						showAlert(responseJson.msg, strings.productsData.title.regular);
					}
				}).catch((error) => {
					addOrUpdateLoadingIndicatorBooleans(
						dispatch,
						loadingIndicator.productsData.main,
						false,
					);
					printInDebugModeOnly(error, 'error-fetchProducts');
				});
		} else {
			showAlert(strings.common.networkError);
		}
	});
};

// NOTE: Use current lat, long values.
const fetchDispensaries = (dispatch) => {
	const body = 'lat=-33.729752&lng=150.836090';
	const url = `${webUrls.dispensaries}?${body}`;
	printInDebugModeOnly(body, 'body-fetchDispensaries');
	NetInfo.isConnected.fetch().then((isConnected) => {
		if (isConnected) {
			addOrUpdateLoadingIndicatorBooleans(dispatch, loadingIndicator.dispensariesList.main, true);
			fetchDataUsingGet(url)
				.then(response => response.json())
				.then((responseJson) => {
					addOrUpdateLoadingIndicatorBooleans(
						dispatch,
						loadingIndicator.dispensariesList.main,
						false,
					);
					if (responseJson.data) {
						saveDispensaries(dispatch, responseJson.data);
					} else {
						showAlert(responseJson.msg, strings.dispensariesList.title.regular);
					}
				}).catch((error) => {
					addOrUpdateLoadingIndicatorBooleans(
						dispatch,
						loadingIndicator.dispensariesList.main,
						false,
					);
					printInDebugModeOnly(error, 'error-fetchDispensaries');
				});
		} else {
			showAlert(strings.common.networkError);
		}
	});
};

const fetchRegister = (dispatch, email, password, navigation) => {
	const body = `email=${email}&password=${password}&device_type=${Platform.OS}&device_token=1234`;
	NetInfo.isConnected.fetch().then((isConnected) => {
		if (isConnected) {
			addOrUpdateLoadingIndicatorBooleans(dispatch, loadingIndicator.signUp.main, true);
			fetchDataUsingPost(body, webUrls.register)
				.then(response => response.json())
				.then((responseJson) => {
					addOrUpdateLoadingIndicatorBooleans(dispatch, loadingIndicator.signUp.main, false);
					if (responseJson.data) {
						AsyncStorage.setItem(strings.common.token, responseJson.data.token);
						saveRegister(dispatch, responseJson.data);
						showAlert(
							responseJson.msg,
							strings.signUp.title.regular,
							() => navigation.navigate(screensNames.DispensaryMap),
						);
					} else {
						showAlert(responseJson.msg, strings.signUp.title.regular);
					}
				}).catch((error) => {
					addOrUpdateLoadingIndicatorBooleans(dispatch, loadingIndicator.signUp.main, false);
					printInDebugModeOnly(error, 'error-fetchRegister');
				});
		} else {
			showAlert(strings.common.networkError);
		}
	});
};

// TODO: fetch real device token.
const fetchLogin = (dispatch, email, password, navigation) => {
	const body = `email=${email}&password=${password}&device_type=${Platform.OS}&device_token=1234`;
	NetInfo.isConnected.fetch().then((isConnected) => {
		if (isConnected) {
			addOrUpdateLoadingIndicatorBooleans(dispatch, loadingIndicator.signIn.main, true);
			fetchDataUsingPost(body, webUrls.login)
				.then(response => response.json())
				.then((responseJson) => {
					addOrUpdateLoadingIndicatorBooleans(dispatch, loadingIndicator.signIn.main, false);
					if (responseJson.data) {
						AsyncStorage.setItem(strings.common.token, responseJson.data.token);
						saveLogin(dispatch, responseJson.data);
						navigation.navigate(screensNames.DispensaryMap);
					} else {
						showAlert(responseJson.msg, strings.signIn.title.regular);
					}
				}).catch((error) => {
					addOrUpdateLoadingIndicatorBooleans(dispatch, loadingIndicator.signIn.main, false);
					printInDebugModeOnly(error, 'error-fetchLogin');
				});
		} else {
			showAlert(strings.common.networkError);
		}
	});
};

export {
	fetchDataUsingPost,
	fetchDataUsingGet,
	fetchRegister,
	fetchLogin,
	fetchDispensaries,
	fetchProducts,
};
