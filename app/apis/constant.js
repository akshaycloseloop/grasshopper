
const url = 'https://v2-dev-apis.grasshopperkiosks.com/';

const weburls = {
	register: `${url}users`,
	login: `${url}users/login`,
	dispensaries: `${url}dispensaries`,
	products: `${url}products`,
};

export default weburls;
