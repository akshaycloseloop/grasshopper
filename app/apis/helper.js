import { AsyncStorage } from 'react-native';
import strings from '../constants/strings';

const getAuthorizationToken = async () => {
	try {
		const token = await AsyncStorage.getItem(strings.common.token);
		return `Bearer ${token}`;
	} catch (error) {
		return error;
	}
};

export {
	// eslint-disable-next-line
	getAuthorizationToken,
};
