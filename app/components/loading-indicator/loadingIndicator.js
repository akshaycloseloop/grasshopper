export default {
	// This indicate that there is some API call going on.
	signIn: {
		main: 'isSigningIn',
	},
	signUp: {
		main: 'isSigningUp',
	},
	dispensariesList: {
		map: 'isLoadingMap',
		main: 'isLoadingDispensaries',
	},
	productsData: {
		main: 'isLoadingProducts',
	},
};
