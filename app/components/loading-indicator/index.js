import React from 'react';
import { ActivityIndicator, View } from 'react-native';
import styles from './styles';
import colors from '../../constants/color';

const sizes = {
	large: 'large',
	small: 'small',
};

const Spinner = () => (
	<View style={styles.loading}>
		<ActivityIndicator size={sizes.large} color={colors.white} />
	</View>
);

export default Spinner;
