import { Alert } from 'react-native';

const showAlert = (message = '', title = '', onOkPress = () => {}, onCancelPress = false) => {
	Alert.alert(
		title,
		message,
		[
			{ text: 'Ok', onPress: onOkPress },
			onCancelPress && { text: 'Cancel', onPress: () => {} },
		],
	);
};

const showDevAlert = (title = 'Development Alert', message = 'Under Development') => {
	showAlert(message, title);
};


export { showAlert, showDevAlert };
