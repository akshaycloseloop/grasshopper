import React from 'react';
import { ImageBackground, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { getTabIconPath } from './helper';

const styles = StyleSheet.create({
	bottomOptionImageyStyle: {
		marginTop: 5,
		width: 22,
		height: 22,
	},
});

const TabIcon = ({ focused, navigation }) => (
	<ImageBackground
		style={styles.bottomOptionImageyStyle}
		source={getTabIconPath(focused, navigation.state.key)}
	/>
);

TabIcon.propTypes = {
	focused: PropTypes.bool.isRequired,
	navigation: PropTypes.shape({
		navigate: PropTypes.func.isRequired,
	}).isRequired,
};

export default TabIcon;
