import Images from '../../images';

export const TAB_TITLE = {
	DISPENSARIES: 'Dispensaries',
	MENU: 'Menu',
	ORDERS: 'Orders',
};

export const getTabIconPath = (focused, tabIcon) => {
	if (tabIcon === TAB_TITLE.DISPENSARIES) {
		if (focused) {
			return Images.dispensarySelected;
		}
		return Images.dispensaryUnSelected;
	} if (tabIcon === TAB_TITLE.MENU) {
		if (focused) {
			return Images.menuSelected;
		}
		return Images.menuUnSelected;
	} if (tabIcon === TAB_TITLE.ORDERS) {
		if (focused) {
			return Images.orderSelected;
		}
		return Images.orderUnSelected;
	}
	if (focused) {
		return Images.dispensarySelected;
	}
	return Images.dispensaryUnSelected;
};
