import React from 'react';
import { Text as RNText, StyleSheet } from 'react-native';
import fontNames from '../../fonts';

const styles = StyleSheet.create({
	defaultStyle: {
		fontFamily: fontNames.neueHaasGroteskBlack,
	},
});

const Text = ({ children, style, ...otherProps }) => (
	<RNText
		style={[styles.defaultStyle, style]}
		{...otherProps}
	>
		{children}
	</RNText>
);

export default Text;
