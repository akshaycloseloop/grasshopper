/* eslint-disable global-require */

const Images = {
	signInWithGoogle: require('./google.png'),
	signInWithFacebook: require('./facebook.png'),
	dispensariesShop: require('./dispensariesShop.png'),
	time: require('./time.png'),
	cpu: require('./cpu.png'),
	like: require('./like.png'),
	liked: require('./liked.png'),
	splashScreen: require('./splash-screen.png'),
	logo: require('./logo.png'),
	locker: require('./locker.png'),
	kiosk: require('./kiosk.png'),
	starEmpty: require('./starEmpty.png'),
	starFilled: require('./starFilled.png'),
	starHalf: require('./starHalf.png'),
	options: require('./options.png'),
	Profile: require('./profile.png'),
	logout: require('./logout.png'),
	enterDispensary: require('./EnterDispensary.png'),
	dispensarySelected: require('./dispensaries-white.png'),
	dispensaryUnSelected: require('./dispensaries-green.png'),
	menuSelected: require('./menu-white.png'),
	menuUnSelected: require('./menu-green.png'),
	orderSelected: require('./orders-white.png'),
	orderUnSelected: require('./orders-green.png'),
	mapView: require('./map-view.png'),
	product1: require('./product-1.png'),
	product2: require('./product-2.png'),
	product3: require('./product-3.png'),
};

/* eslint-enable global-require */
export default Images;
