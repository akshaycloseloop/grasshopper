const colors = {
	// Used as background color in splash screen.
	lightGreen: '#EDF9F4',
	primaryColor: '#095469',
	white: '#fff',
	textInputBackgroundColor: '#DCEEE8',
	buttonBackgroundColor: '#1FC19D',
	noButtonBackgroundColor: '#BBF3DF',
	lightGreenWithOpacity: 'rgba(9,84,105,0.3)',
	greenColor: 'rgba(0,161,155,1)',
	textInputPlaceholder: '#075367',
	green: '#00A19B',
	grey2: '#F9F9F9',
};
export default colors;
