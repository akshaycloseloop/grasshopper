// TODO: Make all the value lowercase.
const fontSize = {
	small: 12,
	DEFAULT_FONT: 14,
	SEMI_LARGE: 16,
	LARGE_FONT: 18,
	mediumLargeFont: 20,
	EXTRA_LARGE_FONT: 22,
	veryLargeFont: 26,
	veryVeryLargeFont: 30,
};

export {
	// eslint-disable-next-line
	fontSize,
};
