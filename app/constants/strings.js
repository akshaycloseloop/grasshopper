const strings = {
	appName: 'Grasshopper',
	common: {
		token: 'Token',
		networkError: 'Please turn on your network and try again',
		platforms: {
			ios: 'ios',
			android: 'android',
		},
		enterEmail: 'Enter Email',
		enterPassword: 'Enter Password',
		logout: {
			title: 'Logout',
			message: 'Do you really want to logout?',
		},
	},
	signIn: {
		title: {
			regular: 'Sign In',
			allCaps: 'SIGN IN',
		},
		createAccount: 'Don’t have an account? Sign Up!',
	},
	signUp: {
		title: {
			regular: 'Sign Up',
			allCaps: 'SIGN UP',
		},
		existingAccountLogin: 'Already have an account? Log in!',
	},
	dispensariesList: {
		view: 'View',
		enterDispensary: 'Enter Dispensary',
		title: {
			regular: 'Dispensaries List',
			allCaps: 'DISPENSARIES LIST',
		},
		map: {
			yourLocation: '',
		},
	},
	productsData: {
		title: {
			regular: 'Products Data',
			allCaps: 'PRODUCTS DATA',
		},
	},
	menu: {
		menuTitle: 'Menu',
		favorites: 'Favorites',
		switchText: 'Show only locker/ kiosk items (fast pickup!):',
	},
};

export default strings;
