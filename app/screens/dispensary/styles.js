import { StyleSheet } from 'react-native';
import colors from '../../constants/color';
import { fontSize } from '../../constants/dimension';
import fontsName from '../../fonts';
import { calloutDispensaryMainImageHeight } from '../dispensaries-map/helper';

const styles = StyleSheet.create({
	container: {
		backgroundColor: colors.white,
	},
	rowContainer: {
		flexDirection: 'row',
	},
	imageBackground: {
		height: calloutDispensaryMainImageHeight,
	},
	likeImage: {
		height: 23,
		width: 25,
		position: 'absolute',
		right: 10,
		top: 9,
	},
	dataContainer: {
		paddingLeft: 11,
		paddingRight: 8,
	},
	nameAndTimeContainer: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		marginTop: 13,
		marginBottom: 4,
	},
	name: {
		color: colors.green,
	},
	timeImage: {
		height: 15,
		width: 15,
		marginEnd: 4,
	},
	timeText: {
		color: colors.green,
		fontSize: fontSize.small,
	},
	addressDistanceFlagsContainer: {
		flexDirection: 'row',
		marginBottom: 8,
		flex: 1,
	},
	addressDistanceContainer: {
		flexDirection: 'row',
		flex: 1,
	},
	addressContaine: {
		flex: 1,
	},
	address: {
		color: colors.green,
		fontSize: fontSize.small,
		fontFamily: fontsName.TradeGothic,
	},
	lineView: {
		backgroundColor: colors.green,
		width: 1,
		marginHorizontal: 10,
	},
	distanceContainer: {
		justifyContent: 'center',
	},
	distance: {
		color: colors.green,
		fontSize: fontSize.small,
		fontFamily: fontsName.TradeGothic,
	},
	iconsContainer: {
		flexDirection: 'row',
		justifyContent: 'flex-end',
		alignItems: 'center',
		flex: 1,
	},
	kiosk: {
		height: 23,
		width: 17,
	},
	locker: {
		width: 30,
		height: 23,
	},
	viewOrEnterDispensaryButtonContainer: {
		paddingTop: 12,
		paddingBottom: 9,
		backgroundColor: colors.buttonBackgroundColor,
		alignItems: 'center',
		justifyContent: 'center',
	},
	viewOrEnterDispensaryButtonText: {
		color: colors.white,
	},
});

export default styles;
