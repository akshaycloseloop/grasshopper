import React, { Component } from 'react';
import { TouchableOpacity, View } from 'react-native';
import Svg, { Image } from 'react-native-svg';
import PropTypes from 'prop-types';
import Text from '../../components/text';
import { screensNames } from '../../routes/helper';
import styles from './styles';
import Images from '../../images';
import { calloutDispensaryMainContainerWidth, calloutDispensaryMainImageHeight } from '../dispensaries-map/helper';
import strings from '../../constants/strings';
import { showDevAlert } from '../../components/alert';

// NOTE: To find if service is available.
const kiosk = 'kiosk';
const locker = 'locker';

class Dispensary extends Component {
	// NOTE: We are wrapping every image in callout to svg,
	// as there is know issue in the react-native-maps library
	// because of which images are not shown.
	// Link to track the issue is
	// "https://github.com/react-native-community/react-native-maps/issues/1870"
	// Wrapping image in Svg container is found in this link
	// https://www.bountysource.com/issues/52594571-image-component-inside-custom-markers-and-callouts-in-android
	// TODO: Update code to have svg view only for Android.
	svgContainer = (href, style, height, width) => (
		<View style={style}>
			<Svg width={width} height={height}>
				<Image
					width={width}
					height={height}
					href={href}
				/>
			</Svg>
		</View>
	)

	renderFavourite = isFavourite => (
		<TouchableOpacity style={styles.likeImage} onPress={() => showDevAlert()}>
			{this.svgContainer(
				isFavourite ? Images.liked : Images.like,
				styles.likeImage,
				25,
				23,
			)}
		</TouchableOpacity>
	)

	isServiceAvailable = (
		services,
		serviceName,
	) => (services.split(',') || []).includes(serviceName);

	render() {
		const { dispensary, navigation, isMarker } = this.props;
		const {
			dispensary_id = -1,
			location_id = -1,
			name = '',
			address = '',
			distance = 0.0,
			timing = '',
			is_favourite = false,
			services = '',
		} = dispensary;
		return (
			<View style={styles.container}>
				<View style={styles.imageBackground}>
					{this.svgContainer(
						Images.dispensariesShop,
						styles.imageBackground,
						calloutDispensaryMainImageHeight,
						calloutDispensaryMainContainerWidth,
					)}
					{this.renderFavourite(is_favourite)}
				</View>
				<View style={styles.dataContainer}>
					<View style={styles.nameAndTimeContainer}>
						<Text style={styles.name}>{name}</Text>
						<View style={styles.rowContainer}>
							{this.svgContainer(
								Images.time,
								styles.timeImage,
								15,
								14,
							)}
							<Text style={styles.timeText}>{timing}</Text>
						</View>
					</View>
					<View style={styles.addressDistanceFlagsContainer}>
						<View style={styles.addressDistanceContainer}>
							<View style={styles.addressContaine}>
								<Text style={styles.address}>{address}</Text>
							</View>
							<View style={styles.lineView} />
							<View style={styles.distanceContainer}>
								<Text
									style={styles.distance}
								>
									{`${distance.toFixed(1)} mi`}
								</Text>
							</View>
						</View>
						<View style={styles.iconsContainer}>
							{this.isServiceAvailable(services, kiosk)
								&& this.svgContainer(
									Images.kiosk,
									styles.kiosk,
									23,
									17,
								)}
							{this.isServiceAvailable(services, locker)
								&& this.svgContainer(
									Images.locker,
									styles.locker,
									23,
									30,
								)}
						</View>
					</View>
				</View>
				<TouchableOpacity
					onPress={() => navigation.navigate(
						screensNames.dispensarySplashScreen,
						{
							dispensaryId: dispensary_id,
							locationId: location_id,
							dispensaryName: name,
						},
					)}
					style={styles.viewOrEnterDispensaryButtonContainer}
				>
					<Text style={styles.viewOrEnterDispensaryButtonText}>
						{isMarker
							? strings.dispensariesList.enterDispensary
							: strings.dispensariesList.view}
					</Text>
				</TouchableOpacity>
			</View>
		);
	}
}

Dispensary.propTypes = {
	dispensary: PropTypes.objectOf(PropTypes.shape).isRequired,
	navigation: PropTypes.shape({
		navigate: PropTypes.func.isRequired,
	}).isRequired,
	isMarker: PropTypes.bool,
};

Dispensary.defaultProps = {
	isMarker: false,
};

export default Dispensary;
