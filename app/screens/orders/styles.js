import { StyleSheet } from 'react-native';
import colors from '../../constants/color';

const styles = StyleSheet.create({
	imageStyle: {
		flex: 1,
		backgroundColor: colors.lightGreen,
	},
});

export default styles;
