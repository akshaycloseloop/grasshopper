import React, { Component } from 'react';
import {
	Image,
	TextInput,
	SafeAreaView,
	View,
	TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import { fetchLogin } from '../../apis';
import styles from './styles';
import Text from '../../components/text';
import Images from '../../images';
import { screensNames } from '../../routes/helper';
import { showAlert } from '../../components/alert';
import Spinner from '../../components/loading-indicator';
import strings from '../../constants/strings';
import colors from '../../constants/color';


class Login extends Component {
	constructor(props) {
		super(props);
		this.state = {
			emailInputText: null,
			passwordInputText: null,
		};
	}

	renderLogo = () => <Image style={styles.logo} source={Images.logo} />

	renderOrText = () => (
		<View style={styles.orView}>
			<View style={styles.lineView} />
			<Text style={styles.orText}>or</Text>
			<View style={styles.lineView} />
		</View>
	)

	renderLoginText = () => (
		<TouchableOpacity
			style={styles.loginView}
			onPress={() => this.validateUserForLogin()}
		>
			<Text style={styles.loginButtonStyle}>
				{strings.signIn.title.allCaps}
			</Text>
			<View style={styles.logInTextBottomLine} />
		</TouchableOpacity>
	)

	validateUserForLogin = () => {
		// eslint-disable-next-line
		const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ; 
		const { emailInputText, passwordInputText } = this.state;
		if (emailInputText && passwordInputText) {
			this.fetchLogin(emailInputText, passwordInputText);
		} else if (!emailInputText) {
			showAlert(strings.common.enterEmail, '');
		} else if (reg.test(emailInputText) === false) {
			showAlert(strings.common.enterEmail, '');
		} else if (!passwordInputText) {
			showAlert(strings.common.enterPassword, '');
		}
	}

	fetchLogin = (email, password) => {
		const { getLoginData, navigation } = this.props;
		getLoginData(email, password, navigation);
	}

	render() {
		const { isLoading, navigation } = this.props;
		return (
			<View style={styles.container}>
				<SafeAreaView style={styles.container}>
					<KeyboardAwareScrollView style={styles.container}>
						{this.renderLogo()}
						<Text style={styles.loginHeadingStyle}>Log In</Text>
						<Image style={styles.imageStyle} source={Images.signInWithGoogle} />
						<Image style={styles.imageStyle} source={Images.signInWithFacebook} />
						{this.renderOrText()}
						<TextInput
							style={styles.textInputStyle}
							placeholder={strings.common.enterEmail}
							placeholderTextColor={colors.textInputPlaceholder}
							onChangeText={text => this.setState({ emailInputText: text })}
						/>
						<TextInput
							style={styles.textInputStyle}
							placeholder={strings.common.enterPassword}
							placeholderTextColor={colors.textInputPlaceholder}
							secureTextEntry
							onChangeText={text => this.setState({ passwordInputText: text })}
						/>
						{this.renderLoginText()}
						<Text
							style={styles.signUpTextStyle}
							onPress={() => navigation.navigate(screensNames.signUp)}
						>
							{strings.signIn.createAccount}
						</Text>
					</KeyboardAwareScrollView>
				</SafeAreaView>
				{isLoading && <Spinner />}
			</View>
		);
	}
}

const mapStateToProps = ({ loading }) => (
	{
		isLoading: loading.isSigningIn,
	}
);

const mapDispatchToProps = dispatch => (
	{
		getLoginData: (
			email,
			password,
			navigation,
		) => fetchLogin(dispatch, email, password, navigation),
	}
);

Login.propTypes = {
	navigation: PropTypes.shape({
		navigate: PropTypes.func.isRequired,
	}).isRequired,
	getLoginData: PropTypes.func.isRequired,
	isLoading: PropTypes.bool,
};

Login.defaultProps = {
	isLoading: false,
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
