import React, { Component } from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import MapView from 'react-native-maps';
import { fetchDispensaries } from '../../apis';
import styles from './styles';
import Dispensary from '../dispensary';
import { screensNames } from '../../routes/helper';

class DispensaryMap extends Component {
	constructor(props) {
		super(props);
		this.state = {
			// NOTE: Uncomment when current location is needed.
			// latitude: 18.4582644,
			// longitude: 73.8198773,
		};
	}

	componentDidMount() {
		const { getDispensaries } = this.props;
		getDispensaries();
		// NOTE: Uncomment when current location is needed.
		// eslint-disable-next-line
		// navigator.geolocation.getCurrentPosition(
		// 	(position) => {
		// 		this.setState({
		// 			latitude: position.coords.latitude,
		// 			longitude: position.coords.longitude,
		// 		}, () => getDispensaries(position.coords.latitude, position.coords.longitude));
		// 	},
		// 	(error) => {
		// 		printInDebugModeOnly(error, 'location-permission-erro');
		// 		showAlert('Please use dispensaries list if you do not want to give location permissions');
		// 	},
		// 	{ enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
		// );
	}

	render() {
		const { dispensaries, navigation } = this.props;
		return (
			<View style={styles.container}>
				<MapView
					style={styles.map}
					region={{
						// NOTE: Currently we are using dummy lat, long as
						// current location.
						latitude: -33.729752,
						longitude: 150.836090,
						latitudeDelta: 0.0922,
						longitudeDelta: 0.0421,
					}}
				>
					{dispensaries.map((dispensaryData) => {
						const {
							lat,
							lng,
							dispensary_id = -1,
							location_id = -1,
							name = '',
						} = dispensaryData;
						return (
							<MapView.Marker
								coordinate={{
									latitude: lat,
									longitude: lng,
								}}
								key={`${lat}${lng}`}
								onCalloutPress={() => navigation.navigate(
									screensNames.dispensarySplashScreen,
									{
										dispensaryId: dispensary_id,
										locationId: location_id,
										dispensaryName: name,
									},
								)}
							>
								<MapView.Callout>
									<Dispensary
										dispensary={dispensaryData}
										navigation={navigation}
										isMarker
									/>
								</MapView.Callout>
							</MapView.Marker>
						);
					})}
				</MapView>
			</View>
		);
	}
}

DispensaryMap.propTypes = {
	getDispensaries: PropTypes.func.isRequired,
	dispensaries: PropTypes.arrayOf(PropTypes.shape).isRequired,
	navigation: PropTypes.shape({
		navigate: PropTypes.func.isRequired,
	}).isRequired,
};

const mapStateToProps = ({ loading, dispensariesData: { dispensaries = [] } }) => (
	{
		isLoading: loading.isSigningIn,
		dispensaries,
	}
);

const mapDispatchToProps = dispatch => (
	{
		getDispensaries: (lat, long) => fetchDispensaries(dispatch, lat, long),
	}
);

export default connect(mapStateToProps, mapDispatchToProps)(DispensaryMap);
