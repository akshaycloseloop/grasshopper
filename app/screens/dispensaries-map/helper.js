import { Dimensions } from 'react-native';
import { isIOS } from '../../helper';

const { width } = Dimensions.get('window');

const calloutDispensaryMainContainerWidth = isIOS ? width - 40 : width - 20;
const calloutDispensaryMainImageHeight = 100;

export {
	calloutDispensaryMainContainerWidth,
	calloutDispensaryMainImageHeight,
};
