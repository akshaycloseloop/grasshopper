import React, { Component } from 'react';
import { ImageBackground, AsyncStorage } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';
import { screensNames } from '../../routes/helper';
import Images from '../../images';
import strings from '../../constants/strings';

class SplashScreen extends Component {
	componentDidMount() {
		const { navigation } = this.props;
		setTimeout(() => {
			AsyncStorage.getItem(strings.common.token).then((token) => {
				if (token) {
					navigation.navigate(screensNames.home);
				} else {
					navigation.navigate(screensNames.Login);
				}
			}).done();
		}, 2000);
	}

	render() {
		return (
			<ImageBackground
				style={styles.imageStyle}
				source={Images.splashScreen}
			/>
		);
	}
}

SplashScreen.propTypes = {
	navigation: PropTypes.shape({
		navigate: PropTypes.func.isRequired,
	}).isRequired,
};

export default SplashScreen;
