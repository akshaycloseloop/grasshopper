import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ImageBackground, View } from 'react-native';
import { connect } from 'react-redux';
import styles from './styles';
import { screensNames, navigationIds } from '../../routes/helper';
import Images from '../../images';
import { fetchProducts } from '../../apis';
import Text from '../../components/text';

class DispensarySplashScreen extends Component {
	componentDidMount() {
		this.fetchRequiredData();
	}

	componentWillReceiveProps() {
		this.fetchRequiredData();
	}

	fetchRequiredData = () => {
		const { navigation } = this.props;
		const dispensaryId = navigation.getParam(navigationIds.dispensaryId, -1);
		const locationId = navigation.getParam(navigationIds.locationId, -1);
		const dispensaryName = navigation.getParam(navigationIds.dispensaryName, '');
		if (dispensaryId !== -1 && locationId !== -1 && dispensaryName !== '') {
			const { getProducts } = this.props;
			getProducts(dispensaryId, locationId);

			setTimeout(() => {
				navigation.navigate(screensNames.productsList, { dispensaryName });
			}, 2000);
		}
	}

	render() {
		const { navigation } = this.props;
		const dispensaryId = navigation.getParam(navigationIds.dispensaryId, -1);
		const locationId = navigation.getParam(navigationIds.locationId, -1);
		const dispensaryName = navigation.getParam(navigationIds.dispensaryName, '');
		if (dispensaryId !== -1 && locationId !== -1 && dispensaryName !== '') {
			return (
				<ImageBackground
					style={styles.imageStyle}
					source={Images.enterDispensary}
				/>
			);
		}
		return (
			<View style={styles.textContainer}>
				<Text style={styles.noDispensaryText}>
					{'Please select a dispensary from map or dispensaries list.'}
				</Text>
			</View>
		);
	}
}

DispensarySplashScreen.propTypes = {
	navigation: PropTypes.shape({
		navigate: PropTypes.func.isRequired,
	}).isRequired,
	getProducts: PropTypes.func.isRequired,
};

const mapDispatchToProps = dispatch => (
	{
		getProducts: (dispensaryId, locationId) => fetchProducts(dispatch, dispensaryId, locationId),
	}
);

export default connect(null, mapDispatchToProps)(DispensarySplashScreen);
