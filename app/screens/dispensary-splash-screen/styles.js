import { StyleSheet } from 'react-native';
import colors from '../../constants/color';

const styles = StyleSheet.create({
	imageStyle: {
		flex: 1,
		backgroundColor: colors.lightGreen,
	},
	textContainer: {
		flex: 1,
		backgroundColor: colors.lightGreen,
		justifyContent: 'center',
		alignItems: 'center',
	},
	noDispensaryText: {
		textAlign: 'center',
	},
});

export default styles;
