import { StyleSheet } from 'react-native';
import colors from '../../constants/color';

const styles = StyleSheet.create({
	page: {
		backgroundColor: colors.lightGreen,
	},
	container: {
		backgroundColor: colors.lightGreen,
		paddingTop: 16,
		paddingStart: 14,
		paddingEnd: 15,
		paddingBottom: 17,
	},
	separator: {
		backgroundColor: colors.lightGreen,
		paddingVertical: 8,
	},
});

export default styles;
