import React, { Component } from 'react';
import { View, FlatList } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import styles from './styles';
import { fetchDispensaries } from '../../apis';
import Dispensary from '../dispensary';
import { menuIcon } from '../../routes/helper';
import Spinner from '../../components/loading-indicator';

class dispensariesList extends Component {
	static navigationOptions = ({ navigation }) => ({
		headerRight: menuIcon(navigation, true),
	});

	componentDidMount() {
		this.fetchDispensaries();
	}

	fetchDispensaries = () => {
		const { getDispensariesData, navigation } = this.props;
		getDispensariesData(navigation);
	}

	render() {
		const { dispensaries, navigation, isLoading } = this.props;
		return (
			<View style={{ flex: 1 }}>
				<FlatList
					data={dispensaries}
					style={styles.page}
					contentContainerStyle={styles.container}
					keyExtractor={({ location_id }) => `key${location_id}`}
					ItemSeparatorComponent={() => <View style={styles.separator} />}
					renderItem={({ item }) => <Dispensary
						dispensary={item}
						navigation={navigation}
						isMarker={false}
					/>}
				/>
				{isLoading && <Spinner />}
			</View>
		);
	}
}

const mapStateToProps = ({ dispensariesData: { dispensaries = [] }, loading }) => (
	{
		dispensaries,
		isLoading: loading.isLoadingDispensaries,
	}
);

const mapDispatchToProps = dispatch => (
	{
		getDispensariesData: () => fetchDispensaries(dispatch),
	}
);

dispensariesList.propTypes = {
	dispensaries: PropTypes.arrayOf(PropTypes.shape),
	navigation: PropTypes.shape({
		navigate: PropTypes.func.isRequired,
	}).isRequired,
	getDispensariesData: PropTypes.func.isRequired,
	isLoading: PropTypes.bool,
};

dispensariesList.defaultProps = {
	dispensaries: [],
	isLoading: false,
};

export default connect(mapStateToProps, mapDispatchToProps)(dispensariesList);
