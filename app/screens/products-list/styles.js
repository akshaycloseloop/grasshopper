import { StyleSheet } from 'react-native';
import colors from '../../constants/color';
import fontsName from '../../fonts';
import { fontSize } from '../../constants/dimension';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: colors.lightGreen,
	},
	switchText: {
		color: colors.greenColor,
		fontSize: 12,
		fontWeight: 'bold',
		marginTop: 15,
		fontFamily: fontsName.neueHaasGroteskBlack,
		lineHeight: 15,
	},
	switchView: {
		flexDirection: 'row',
		marginHorizontal: 34,
	},
	switch: {
		transform: [{ scaleX: 0.7 },
			{
				scaleY: 0.7,
			},
		],
		marginTop: 8,
	},
	contentContainer: {
		paddingLeft: 14,
		paddingRight: 15,
		paddingTop: 12,
	},
	productSeparator: {
		height: 1,
		marginVertical: 8,
		backgroundColor: colors.lightGreen,
	},
	productContainer: {
		backgroundColor: colors.grey2,
		flexDirection: 'row',
		flex: 1,
	},
	dataContainer: {
		paddingLeft: 16,
		paddingTop: 10,
		paddingBottom: 11,
		flexDirection: 'row',
		flex: 2,
	},
	nameRatingPriceContainer: {
		alignItems: 'flex-start',
		flex: 2,
	},
	name: {
		color: colors.primaryColor,
		textAlign: 'left',
		fontSize: fontSize.mediumLargeFont,
		paddingVertical: 4,
	},
	starsContainer: {
		paddingVertical: 4,
	},
	starImage: {
		width: 16,
		height: 16,
		color: 'yellow',
	},
	dropdownPriceView: {
		flexDirection: 'row',
		paddingTop: 16,
	},
	dropdownContainer: {
		borderWidth: 1,
		height: 22,
		width: 68,
		borderColor: colors.buttonBackgroundColor,
		borderRadius: 4,
	},
	priceTextContainer: {
		justifyContent: 'center',
		flex: 1,
		marginLeft: 16,
	},
	priceText: {
		color: colors.primaryColor,
		fontSize: fontSize.veryLargeFont,
		fontFamily: fontsName.ChaletComprimeCologneEight,
		textAlign: 'left',
	},
	categoryServicesAndFavouriteContainer: {
		justifyContent: 'space-between',
		alignItems: 'flex-end',
		flex: 1,
		paddingRight: 11,
	},
	category: {
		color: colors.primaryColor,
		fontSize: fontSize.LARGE_FONT,
		fontFamily: fontsName.ChaletComprimeCologneEight,
		textAlign: 'center',
		paddingTop: 8,
	},
	iconsContainer: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignContent: 'center',
	},
	kiosk: {
		height: 23,
		width: 17,
		marginRight: 8,
	},
	locker: {
		width: 30,
		height: 23,
		marginLeft: 8,
	},
	favouriteIcon: {
		height: 21.4,
		width: 23.72,
	},
	imageContainer: {
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: colors.white,
		flex: 1,
	},
	productImage: {
		height: 72,
		width: 72,
	},
});

export default styles;
