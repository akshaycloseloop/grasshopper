import React, { Component } from 'react';
import {
	View,
	Switch,
	FlatList,
	Image,
	TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import StarRating from 'react-native-star-rating';
import { Dropdown } from 'react-native-material-dropdown';
import styles from './styles';
import strings from '../../constants/strings';
import Images from '../../images';
import Spinner from '../../components/loading-indicator';
import Text from '../../components/text';
import fontsName from '../../fonts';
import { fontSize } from '../../constants/dimension';
import { navigationIds } from '../../routes/helper';
import { showDevAlert } from '../../components/alert';

// NOTE: To find if service is available.
const kiosk = 'kiosk';
const locker = 'locker';

class ProductsList extends Component {
	static navigationOptions = ({ navigation }) => {
		const dispensaryName = navigation.getParam(navigationIds.dispensaryName, '');
		return ({
			headerTitle: dispensaryName,
			headerRight: null,
		});
	};

	constructor(props) {
		super(props);
		this.state = {
			priceToShow: undefined,
		};
	}

	renderSwitch = () => (
		<TouchableOpacity
			style={styles.switchView}
			onPress={() => showDevAlert()}
		>
			<Text style={styles.switchText}>{strings.menu.switchText}</Text>
			<Switch
				style={styles.switch}
				onValueChange={() => showDevAlert()}
			/>
		</TouchableOpacity>
	);


	isServiceAvailable = (
		services,
		serviceName,
	) => (services.split(',') || []).includes(serviceName);

	renderCategoryIconsAndFavourite = ({ product_category, services, is_favourite }) => {
		const isKioskAvailable = this.isServiceAvailable(services, kiosk);
		const isLockerAvailable = this.isServiceAvailable(services, locker);
		return (
			<View style={styles.categoryServicesAndFavouriteContainer}>
				<Text style={styles.category}>{product_category}</Text>
				<View style={styles.iconsContainer}>
					{isKioskAvailable && <Image
						source={Images.kiosk}
						style={[styles.kiosk, !isLockerAvailable ? { marginRight: 0 } : {}]}
					/>}
					{isLockerAvailable && <Image
						source={Images.locker}
						style={[styles.locker, !isKioskAvailable ? { marginLeft: 0 } : {}]}
					/>}
				</View>
				<Image style={styles.favouriteIcon} source={is_favourite ? Images.liked : Images.like} />
			</View>
		);
	}

	renderNameRatingsPrice= ({ product_name, rating, pricing = [] }) => {
		const { priceToShow } = this.state;
		return (
			<View style={styles.nameRatingPriceContainer}>
				<Text style={styles.name}>{product_name}</Text>
				<View style={styles.starsContainer}>
					<StarRating
						disabled={false}
						maxStars={5}
						rating={rating}
						emptyStar={Images.starEmpty}
						fullStar={Images.starFilled}
						halfStar={Images.starHalf}
						starStyle={{
							height: 20,
							width: 20,
						}}
					/>
				</View>
				<View style={styles.dropdownPriceView}>
					<Dropdown
						data={pricing}
						valueExtractor={({ weight }) => weight}
						containerStyle={styles.dropdownContainer}
						dropdownOffset={{ top: 0, left: 0 }}
						inputContainerPadding={0}
						inputContainerStyle={{
							borderBottomColor: 'transparent',
						}}
						itemTextStyle={{
							fontFamily: fontsName.neueHaasGroteskBlack,
							fontSize: fontSize.small,
						}}
						labelTextStyle={{
							fontFamily: fontsName.neueHaasGroteskBlack,
							fontSize: fontSize.small,
						}}
						fontSize={fontSize.small}
						style={{
							paddingStart: 16,
							fontFamily: fontsName.neueHaasGroteskBlack,
							fontSize: fontSize.small,
						}}
						// NOTE: Need to update price as per the dropdown value.
						// onChangeText={(value, index, data)
						// => this.setState({ priceToShow: data[index].price })}
					/>
					<View style={styles.priceTextContainer}>
						<Text style={styles.priceText}>{`$${priceToShow || pricing[0].price}`}</Text>
					</View>
				</View>
			</View>
		);
	}

	renderFavouriteProduct = item => (
		<View style={styles.productContainer}>
			<View style={styles.dataContainer}>
				{this.renderNameRatingsPrice(item)}
				{this.renderCategoryIconsAndFavourite(item)}
			</View>
			<View style={styles.imageContainer}>
				<Image
					style={styles.productImage}
					source={Images.product3}
				/>
			</View>
		</View>
	)

	render() {
		const { favouriteProducts, isLoading } = this.props;
		return (
			<View style={styles.container}>
				{this.renderSwitch()}
				<FlatList
					data={favouriteProducts}
					contentContainerStyle={styles.contentContainer}
					ItemSeparatorComponent={() => <View style={styles.productSeparator} />}
					keyExtractor={({ id }) => `${id}`}
					renderItem={({ item }) => this.renderFavouriteProduct(item)}
				/>
				{isLoading && <Spinner />}
			</View>
		);
	}
}

const mapStateToProps = ({ loading, productsData: { favourite_products = [] } }) => (
	{
		isLoading: loading.isLoadingProducts,
		favouriteProducts: favourite_products,
	}
);

ProductsList.propTypes = {
	favouriteProducts: PropTypes.arrayOf(PropTypes.shape),
	navigation: PropTypes.shape({
		navigate: PropTypes.func.isRequired,
	}).isRequired,
	isLoading: PropTypes.bool,
};

ProductsList.defaultProps = {
	favouriteProducts: {},
	isLoading: false,
};

export default connect(mapStateToProps)(ProductsList);
