import React, { Component } from 'react';
import {
	Image,
	View,
	TextInput,
	SafeAreaView,
	TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import { fetchRegister } from '../../apis';
import styles from './styles';
import Text from '../../components/text';
import Spinner from '../../components/loading-indicator';
import Images from '../../images';
import { screensNames } from '../../routes/helper';
import { showAlert } from '../../components/alert';
import strings from '../../constants/strings';
import colors from '../../constants/color';

class SignUp extends Component {
	constructor(props) {
		super(props);
		this.state = {
			emailInputText: undefined,
			passwordInputText: undefined,
		};
	}

	renderLogo = () => <Image style={styles.logo} source={Images.logo} />

	renderOrText = () => (
		<View style={styles.orView}>
			<View style={styles.lineView} />
			<Text style={styles.orText}>or</Text>
			<View style={styles.lineView} />
		</View>
	)

	renderSignUpText = () => (
		<TouchableOpacity
			style={styles.signUpView}
			onPress={() => this.validateUserForRegister()}
		>
			<Text style={styles.signUpButtonStyle}>
				{strings.signUp.title.allCaps}
			</Text>
			<View style={styles.signUpTextBottomLine} />
		</TouchableOpacity>
	)

	validateUserForRegister = () => {
		// eslint-disable-next-line
		const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		const { emailInputText, passwordInputText } = this.state;
		if (emailInputText && passwordInputText) {
			this.fetchRegister(emailInputText, passwordInputText);
		} else if (!emailInputText) {
			showAlert(strings.common.enterEmail);
		} else if (reg.test(emailInputText) === false) {
			showAlert(strings.common.enterEmail);
		} else if (!passwordInputText) {
			showAlert(strings.common.enterPassword);
		}
	}

	fetchRegister = (email, password) => {
		const { getRegisterData, navigation } = this.props;
		getRegisterData(email, password, navigation);
	}

	render() {
		const { isLoading, navigation } = this.props;
		return (
			<View style={styles.container}>
				<SafeAreaView style={styles.container}>
					<KeyboardAwareScrollView style={styles.container}>
						{this.renderLogo()}
						<Text style={styles.signUpHeadingStyle}>Sign Up</Text>
						<Image style={styles.imageStyle} source={Images.signInWithGoogle} />
						<Image style={styles.imageStyle} source={Images.signInWithFacebook} />
						{this.renderOrText()}
						<TextInput
							style={styles.textInputStyle}
							placeholder={strings.common.enterEmail}
							placeholderTextColor={colors.textInputPlaceholder}
							onChangeText={text => this.setState({ emailInputText: text })}
						/>
						<TextInput
							style={styles.textInputStyle}
							placeholder={strings.common.enterPassword}
							placeholderTextColor={colors.textInputPlaceholder}
							secureTextEntry
							onChangeText={text => this.setState({ passwordInputText: text })}
						/>
						{this.renderSignUpText(navigation)}
						<Text
							style={styles.signInTextStyle}
							onPress={() => navigation.navigate(screensNames.Login)}
						>
							{strings.signUp.existingAccountLogin}
						</Text>
					</KeyboardAwareScrollView>
				</SafeAreaView>
				{isLoading && <Spinner />}
			</View>
		);
	}
}

const mapStateToProps = ({ loading }) => (
	{
		isLoading: loading.isSigningUp,
	}
);

const mapDispatchToProps = dispatch => (
	{
		getRegisterData: (
			email,
			password,
			navigation,
		) => fetchRegister(dispatch, email, password, navigation),
	}
);

SignUp.propTypes = {
	navigation: PropTypes.shape({
		navigate: PropTypes.func.isRequired,
	}).isRequired,
	getRegisterData: PropTypes.func.isRequired,
	isLoading: PropTypes.bool,
};

SignUp.defaultProps = {
	isLoading: false,
};

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
