import { StyleSheet } from 'react-native';
import colors from '../../constants/color';
import { fontSize } from '../../constants/dimension';
import fontsName from '../../fonts';

const styles = StyleSheet.create({
	container: {
		backgroundColor: colors.lightGreen,
		flex: 1,
	},
	logo: {
		height: 104,
		width: 161,
		marginTop: 68.41,
		marginBottom: 30,
		alignSelf: 'center',
	},
	signUpText: {
		alignSelf: 'center',
		color: colors.primaryColor,
		fontSize: fontSize.EXTRA_LARGE_FONT,
		lineHeight: 28,
		marginBottom: 18,
	},
	orText: {
		alignSelf: 'center',
		color: colors.primaryColor,
		fontSize: fontSize.LARGE_FONT,
		marginLeft: 10,
		marginRight: 10,
		fontWeight: 'bold',
		fontFamily: fontsName.neueHaasGroteskBlack,
	},
	orView: {
		flexDirection: 'row',
		justifyContent: 'center',
		marginVertical: 15,
	},
	lineView: {
		width: 68,
		height: 1,
		alignSelf: 'center',
		backgroundColor: colors.primaryColor,
	},
	textInputStyle: {
		height: 45,
		backgroundColor: colors.textInputBackgroundColor,
		color: colors.primaryColor,
		marginHorizontal: 37.61,
		paddingHorizontal: 17,
		paddingTop: 11,
		paddingBottom: 6,
		fontSize: fontSize.veryVeryLargeFont,
		opacity: 0.8,
		marginVertical: 10,
		fontFamily: fontsName.ChaletComprimeCologneEight,
	},
	signUpView: {
		backgroundColor: colors.buttonBackgroundColor,
		marginLeft: 96.61,
		marginRight: 96.61,
		paddingHorizontal: 30,
		marginVertical: 20,
		paddingVertical: 10,
		justifyContent: 'center',
	},
	signUpTextBottomLine: {
		height: 2,
		backgroundColor: colors.white,
	},
	signUpButtonStyle: {
		alignSelf: 'center',
		color: colors.white,
		fontSize: fontSize.veryVeryLargeFont,
		fontFamily: fontsName.ChaletComprimeCologneEight,
	},
	signUpHeadingStyle: {
		alignSelf: 'center',
		color: colors.primaryColor,
		marginTop: 30,
		marginBottom: 10,
		fontSize: fontSize.EXTRA_LARGE_FONT,
		paddingVertical: 10,
		fontFamily: fontsName.neueHaasGroteskBlack,
	},
	signInTextStyle: {
		color: colors.primaryColor,
		fontWeight: 'normal',
		alignSelf: 'center',
		marginTop: 10,
		marginBottom: 20,
		fontSize: 14,
		fontFamily: fontsName.TradeGothic,
		letterSpacing: 1.75,
	},
	imageStyle: {
		height: 40,
		alignSelf: 'center',
		marginHorizontal: 31,
		marginVertical: 10,
	},
});

export default styles;
