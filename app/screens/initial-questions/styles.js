import { StyleSheet } from 'react-native';
import colors from '../../constants/color';
import { fontSize } from '../../constants/dimension';
import fontsName from '../../fonts';

const styles = StyleSheet.create({
	container: {
		backgroundColor: colors.lightGreen,
		flex: 1,
	},
	logo: {
		height: 65,
		width: 98,
		marginTop: 45,
		borderWidth: 1,
		alignSelf: 'center',
	},
	textStyle: {
		alignSelf: 'center',
		color: colors.primaryColor,
		marginTop: 35,
		fontSize: fontSize.SEMI_LARGE,
		paddingVertical: 10,
		letterSpacing: 0.57,
		fontFamily: fontsName.neueHaasGroteskBlack,
		lineHeight: 20,
	},
	issuedIdTextStyle: {
		alignSelf: 'center',
		color: colors.primaryColor,
		marginBottom: 10,
		fontSize: fontSize.DEFAULT_FONT,
		letterSpacing: 0.57,
		fontFamily: fontsName.neueHaasGroteskBlack,
		lineHeight: 20,
	},
	yesNoButtonView: {
		flexDirection: 'row',
		height: 55,
		alignSelf: 'center',
		marginTop: 10,
	},
	yesButton: {
		backgroundColor: colors.buttonBackgroundColor,
		flex: 1,
		marginRight: 30,
		textAlign: 'center',
		paddingVertical: 16,
		fontFamily: fontsName.ChaletComprimeCologneEight,
		fontSize: 30,
		color: colors.white,
	},
	noButton: {
		backgroundColor: colors.noButtonBackgroundColor,
		flex: 1,
		marginLeft: 30,
		marginRight: 27,
		textAlign: 'center',
		paddingVertical: 16,
		fontFamily: fontsName.ChaletComprimeCologneEight,
		fontSize: 30,
		color: colors.white,
	},
	textInputStyle: {
		height: 45,
		backgroundColor: colors.textInputBackgroundColor,
		color: colors.primaryColor,
		marginHorizontal: 41,
		paddingHorizontal: 17,
		paddingTop: 11,
		paddingBottom: 6,
		fontSize: fontSize.veryVeryLargeFont,
		marginTop: 15,
		marginBottom: 32,
		lineHeight: 29,
		letterSpacing: 1.07,
		fontFamily: fontsName.ChaletComprimeCologneEight,
	},
	phoneNoTextStyle: {
		alignSelf: 'center',
		color: colors.primaryColor,
		marginBottom: 10,
		fontSize: fontSize.SEMI_LARGE,
		letterSpacing: 0.57,
		fontFamily: fontsName.neueHaasGroteskBlack,
		lineHeight: 20,
	},
	doneView: {
		backgroundColor: colors.buttonBackgroundColor,
		marginHorizontal: 30,
		height: 55,
		paddingHorizontal: 136,
		marginVertical: 20,
	},
	doneTextBottomLine: {
		height: 2,
		backgroundColor: colors.white,
	},
	doneButtonStyle: {
		alignSelf: 'center',
		color: colors.white,
		paddingTop: 15,
		fontSize: fontSize.veryVeryLargeFont,
		fontFamily: fontsName.ChaletComprimeCologneEight,
		letterSpacing: 1.07,
	},
});

export default styles;
