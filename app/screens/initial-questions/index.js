import React, { Component } from 'react';
import {
	Image,
	SafeAreaView,
	ScrollView,
	Text,
	View,
	TextInput,
} from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';
import { screensNames } from '../../routes/helper';

class initialQuestions extends Component {
	renderLogo = () => <Image style={styles.logo} />

	renderYesNoButton = () => (
		<View style={styles.yesNoButtonView}>
			<Text
				style={styles.noButton}
			>
				{'No'}
			</Text>
			<Text
				style={styles.yesButton}
			>
				{'Yes'}
			</Text>
		</View>
	)

	renderDoneButton = navigation => (
		<View style={styles.doneView}>
			<Text
				style={styles.doneButtonStyle}
				onPress={() => navigation.navigate(screensNames.DispensaryMap)}
			>
				{'DONE'}
			</Text>
			<View style={styles.doneTextBottomLine} />
		</View>
	)

	render() {
		const { navigation } = this.props;
		return (
			<SafeAreaView style={styles.container}>
				<ScrollView style={styles.container}>
					{this.renderLogo()}
					<Text style={styles.textStyle}>Are you over 21?</Text>
					{this.renderYesNoButton()}
					<Text style={styles.textStyle}>What is your full legal name?</Text>
					<Text style={styles.issuedIdTextStyle}>(This must match your government issued ID)</Text>
					<TextInput
						style={styles.textInputStyle}
						placeholder="John Lee"
						placeholderTextColor="#075367"
					/>
					<Text style={styles.phoneNoTextStyle}>What is your phone number?</Text>
					<TextInput
						style={styles.textInputStyle}
						placeholder="(555)555-5555"
						placeholderTextColor="#075367"
					/>
					{this.renderDoneButton(navigation)}
				</ScrollView>
			</SafeAreaView>
		);
	}
}

initialQuestions.propTypes = {
	navigation: PropTypes.shape({
		navigate: PropTypes.func.isRequired,
	}).isRequired,
};

export default initialQuestions;
