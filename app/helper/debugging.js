const DATA_STARTS_PRINTING_WITH = '#####';

/* eslint-disable */
export const printInDebugModeOnly = (data, textToAppend = DATA_STARTS_PRINTING_WITH) => {
  if (__DEV__) {
    console.log(textToAppend, data);
  }
};

export const printWarningInDebugModelOnly = (data, textToAppend = DATA_STARTS_PRINTING_WITH) => {
  if (__DEV__) {
    console.warn(JSON.stringify(textToAppend, 2), data);
  }
}
/* eslint-disable */
