import { Platform } from 'react-native';
import strings from '../constants/strings';

const isIOS = Platform.OS === strings.common.platforms.ios;

export {
	// eslint-disable-next-line
	isIOS,
};
