import React from 'react';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import logger from 'redux-logger';
import rootReducer from './redux/reducer';
import Route from './routes';

const middlewares = [];
// eslint-disable-next-line
if (__DEV__) {
	middlewares.push(applyMiddleware(logger));
}

const store = createStore(rootReducer, ...middlewares);

const App = () => (
	<Provider store={store}>
		<Route />
	</Provider>
);

export default App;
