import React, { Component } from 'react';
import { TouchableOpacity, Image, AsyncStorage } from 'react-native';
import {
	createAppContainer,
	createSwitchNavigator,
	createBottomTabNavigator,
	createStackNavigator,
} from 'react-navigation';
import NavigationService from './NavigationService';
import SplashScreen from '../screens/splash-screen';
import Login from '../screens/login';
import SignUp from '../screens/signUp';
import InitialQuestions from '../screens/initial-questions';
import DispensariesList from '../screens/dispensaries-list';
import DispensarySplashScreen from '../screens/dispensary-splash-screen';
import DispensaryMap from '../screens/dispensaries-map';
import Orders from '../screens/orders';
import colors from '../constants/color';
import TabIcon from '../components/tab-icons';
import strings from '../constants/strings';
import styles from './styles';
import Images from '../images';
import { screensNames, menuIcon } from './helper';
import ProductsList from '../screens/products-list';
import { showAlert } from '../components/alert';
import { fontSize } from '../constants/dimension';
import fontsName from '../fonts';

const logout = (navigation) => {
	AsyncStorage.removeItem(strings.common.token);
	navigation.navigate(screensNames.Login);
};

const logoutIcon = navigation => (
	<TouchableOpacity
		style={styles.headerIcon}
		onPress={() => showAlert(
			strings.common.logout.message,
			strings.common.logout.title,
			() => logout(navigation),
			true,
		)}
	>
		<Image source={Images.logout} style={styles.logoutIcon} />
	</TouchableOpacity>
);

const DispensaryStack = createStackNavigator(
	{
		DispensaryMap,
		DispensariesList,
	},
	{
		defaultNavigationOptions: ({ navigation }) => ({
			headerStyle: {
				borderBottomWidth: 2,
				borderBottomColor: colors.primaryColor,
				paddingEnd: 15,
				paddingStart: 15,
				paddingVertical: 15,
			},
			headerLeft: logoutIcon(navigation),
			headerRight: menuIcon(navigation),
		}),
	},
);

const ProductStack = createStackNavigator(
	{
		ProductsList,
	},
	{
		defaultNavigationOptions: ({ navigation }) => ({
			headerStyle: {
				borderBottomWidth: 2,
				borderBottomColor: colors.primaryColor,
				paddingEnd: 15,
				paddingStart: 15,
				paddingVertical: 15,
			},
			headerTitleStyle: {
				fontSize: fontSize.SEMI_LARGE,
				fontFamily: fontsName.neueHaasGroteskBlack,
				color: colors.primaryColor,
			},
			headerLeft: logoutIcon(navigation),
			headerRight: menuIcon(navigation),
		}),
	},
);

const MenuStack = createSwitchNavigator(
	{
		DispensarySplashScreen,
		ProductStack,
	},
	{
		defaultNavigationOptions: ({ navigation }) => ({
			headerStyle: {
				borderBottomWidth: 2,
				borderBottomColor: colors.primaryColor,
				paddingEnd: 15,
				paddingStart: 15,
				paddingVertical: 15,
			},
			headerLeft: logoutIcon(navigation),
		}),
	},
);

const HomeStack = createBottomTabNavigator(
	{
		Dispensaries: DispensaryStack,
		Menu: MenuStack,
		Orders,
	},
	{
		defaultNavigationOptions: ({ navigation }) => ({
			tabBarVisible: true,
			// eslint-disable-next-line
			tabBarIcon: ({ focused }) => (
				<TabIcon
					focused={focused}
					navigation={navigation}
				/>
			),
		}),
		tabBarOptions: {
			activeTintColor: colors.white,
			inactiveTintColor: colors.primaryColor,
			activeBackgroundColor: colors.primaryColor,
			inactiveBackgroundColor: colors.white,
		},
	},
);
const mainRoute = createSwitchNavigator(
	{
		SplashScreen,
		Login,
		SignUp,
		InitialQuestions,
		HomeStack,
	},
	{
		// Fot debugging purpose, we need not to revisit splash screen,
		// everytime, we can directly go to login.
		// eslint-disable-next-line
	  	initialRouteName: __DEV__ ? 'SplashScreen' : 'SplashScreen',
	},
);

const TopLevelNavigator = createAppContainer(mainRoute);

// We are adding Navigation services to our main Route, to support
// navigation without having navigation in the props. We should not
// use it with out navigation props, but sometimes it is useful.
class Route extends Component {
	render() {
		return (
			<TopLevelNavigator
				ref={(navigatorRef) => {
					NavigationService.setTopLevelNavigator(navigatorRef);
				}}
			/>
		);
	}
}

export default Route;
