import React from 'react';
import { TouchableOpacity, Image } from 'react-native';
import styles from './styles';
import Images from '../images';

const screensNames = {
	splashScreen: 'SplashScreen',
	Login: 'Login',
	signUp: 'SignUp',
	initialQuestions: 'InitialQuestions',
	DispensaryMap: 'DispensaryMap',
	menu: 'Menu',
	orders: 'Orders',
	home: 'HomeStack',
	dispensarySplashScreen: 'DispensarySplashScreen',
	enterDispensaries: 'EnterDispensaries',
	dispensariesList: 'DispensariesList',
	productsList: 'ProductsList',
};

const navigationIds = {
	dispensaryId: 'dispensaryId',
	locationId: 'locationId',
	dispensaryName: 'dispensaryName',
};

const menuIcon = (navigation, shouldShowMapViewIcon) => (
	<TouchableOpacity
		style={styles.headerIcon}
		onPress={() => navigation.navigate(
			shouldShowMapViewIcon
				? screensNames.DispensaryMap
				: screensNames.dispensariesList,
		)}
	>
		<Image
			source={shouldShowMapViewIcon ? Images.mapView : Images.options}
			style={styles.headerIcon}
		/>
	</TouchableOpacity>
);

export {
	screensNames,
	menuIcon,
	navigationIds,
};
