import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
	headerIcon: {
		height: 35,
		width: 35,
	},
	logoutIcon: {
		height: 25,
		width: 25,
		margin: 5,
	},
});

export default styles;
