import { isIOS } from '../helper';

const fontsName = {
	neueHaasGroteskBlack: isIOS ? 'Neue Haas Grotesk Text Pro' : 'NeueHaasGroteskBlack',
	ChaletComprimeCologneEight: isIOS ? 'ChaletComprime' : 'ChaletComprimeCologneEight',
	TradeGothic: isIOS ? 'TradeGothic' : 'TradeGothic',
};

export default fontsName;
